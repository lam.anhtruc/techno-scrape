import configparser
from instabot import Bot
from pathlib import Path
from typing import Union


class ImgUpload(object):

    def __init__(self):
        self.config = self.load_config(config_path=Path('config.ini'))
        self.main_path = Path(self.config["PATHS"]["PROJECT_PATH"]) / "up"
        self.username = self.config["CREDENTIALS"]["USER_NAME"]
        self.password = self.config["CREDENTIALS"]["PASSWORD"]
        self.bot = Bot()
        self.bot.login(username=self.username, password=self.password)

    def load_config(self, config_path: Union[str, Path]) -> configparser.RawConfigParser:
        """
        Loads the config file.

        Note:
            In case of a FileNotFoundError due to the nature of network drives this could also be a hint towards an
            incorrect or missing drive mapping.

        Args:
            config_path (Union[str, Path], optional): If provided, specifies the config file path to be loaded.

        Returns:
            configparser.RawConfigParser: The loaded config.

        Raises:
            FileNotFoundError: If the specified config file could not be found at the target location.
        """

        if not config_path.exists():
            error_msg = f'Attempting to load the config file {config_path}, while this file could not be found!'
            self.logger.error(error_msg)
            raise FileNotFoundError(error_msg)

        config = configparser.RawConfigParser()
        config.read(str(config_path))

        return config

    def upload(self, img_path: Path):
        self.bot.upload_photo(str(img_path), caption="Test")


if __name__ == '__main__':
    scotty = ImgUpload()
    example_img_path = scotty.main_path / "example_img.jpeg"
    scotty.upload(img_path=example_img_path)




