import warnings
warnings.simplefilter(action='ignore', category=FutureWarning)

import time
import requests
import shutil
import pandas as pd

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.action_chains import ActionChains

def scroll_to(driver, ele):
    """Scrolls up to a given Element"""
    actions = ActionChains(driver)
    actions.move_to_element(eve).perform()
    time.sleep(0.5)

def save_pic(scr, filename):
    """Saves image-file from given url"""
    headers = {'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36'}
    pic = requests.get(act['scr'], headers=headers, stream=True)
    pic.raw.decode_content = True
    with open(filename, 'wb') as f:
        shutil.copyfileobj(pic.raw, f)

date = "2022-04-05"
url = f"https://ra.co/events/de/berlin?week={date}"
output = pd.DataFrame()

# Webdriver-element
driver = webdriver.Chrome(executable_path="chromedriver")
driver.maximize_window()
driver.get(url)

# Find relevant HTML-elements...
current = driver.find_element(by=By.XPATH, value="//div[@class='Box-omzyfs-0 fYkcJU']")
events = current.find_elements(by=By.XPATH, value=".//div[@class='Box-omzyfs-0 gExFDv']")

# ... and extracts data from them
for eve in events:

    # Scroll up to 'eve' to load the picture
    scroll_to(driver, eve)

    # Check if image is available
    try:
        scr = eve.find_element(by=By.XPATH, value=".//img").get_attribute("src")
    except:
        scr = "NoImg!"


    # Extract information from site
    act = {
        "name": eve.find_element(by=By.XPATH, value=".//h3[@class='Box-omzyfs-0 Heading__StyledBox-sc-120pa9w-0 fhMVGI']").text,
        "location": eve.find_element(by=By.XPATH, value =".//div[@class='Box-omzyfs-0 Alignment-sc-1fjm9oq-0 esiJmi']").text.split('\n')[0],
        "scr": scr
    }

    output = output.append(act, ignore_index=True)
    time.sleep(0.5)
    # Safe picture (if available)
    if not scr == "NoImg!":
        save_pic(act["scr"], f"jpgs/{act['name']}_{date}.jpg")

# Safe results
output.to_excel("output.xlsx")
output.to_csv("output.csv")
driver.close()
